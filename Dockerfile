FROM debian:bookworm-backports

ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update -y; apt-get full-upgrade -y; apt-get install -y \
	wget \
	curl \
	git \
	dmidecode \
	ca-certificates \
	rsync \
	systemd systemd-sysv; \
	apt-get clean ; \
	rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ; \
	rm -rf /lib/systemd/system/multi-user.target.wants/* ; \
	rm -rf /etc/systemd/system/*.wants/* ; \
	rm -rf /lib/systemd/system/local-fs.target.wants/* ; \
	rm -rf /lib/systemd/system/sockets.target.wants/*udev* ; \
	rm -rf /lib/systemd/system/sockets.target.wants/*initctl* ; \
	rm -rf /lib/systemd/system/sysinit.target.wants/systemd-tmpfiles-setup* ; \
	rm -rf /lib/systemd/system/systemd-update-utmp*

RUN curl -L https://bootstrap.saltproject.io | sh -s --
RUN mkdir -p /srv/salt/states; mkdir -p /srv/salt/pillar
COPY minion /etc/salt/minion
